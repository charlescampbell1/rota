# Rota

An application to assist in creating rota's and caclulating staff hours.

## Development

### Run Application Locally

```bash
> rake db:setup
> rake db:migrate

> bin rails/server
```

- Application will start on port 3000

### Running Test Suites

```bash
> bundle exec rspec
OR
> rspec
```

- Test suite will run in the terminal locally

### Dockerized Application

#### Run Application

```bash
> docker-compose build .
> docker-compuse up
```

#### Run Tests

```bash
> make test
> make lint
```

- Application will start in a docker container on port 3000
- Test suite will run in a docker container in the terminal
